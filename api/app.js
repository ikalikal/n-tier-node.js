const express = require('express');
const app = express();
const bodyParser = require('body-parser');


//routes
const bookRouter = require('./routes/bookRouter');

//registering routes
app.use(bodyParser.json());
app.use('/api/book', bookRouter);

app.listen(3000, ()=>{
    console.log("Server on ");
});
'use strict';

var express = require('express');
var router = express.Router();

const BookController = require('../controllers/BookController');
const book = new BookController();

// GET catalog home page.
router.get('/', book.index);
// // POST request for creating Book.
router.post('/', book.create);

// // GET request for one Book.
router.get('/:id', book.detail);

// // PUT request for one Book.
router.put('/:id', book.update);

// // DELETE request to delete Book.
router.delete('/:id/', book.delete);

module.exports = router;

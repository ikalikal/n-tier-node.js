"use strict"

// var Book = require('../models/book');
const responseHelper = require('../libraries/responseHelper.js');
const Joi = require('joi');
const bookForm = require('../forms/bookForm.js');

const BookService = require('../../bll/repositories/BookService');
const bookService = new BookService.BookService();
var http = require('http');

class BookController{   
    constructor(){        
    }

    async index(req, res, next){            
        var data = await bookService.getList();

        // var books = bookService.getList();            
        responseHelper.returnSuccess(res, 200, data);
    };

    async create(req, res, next){
        const result = Joi.validate(req.body, bookForm);    
        if(result.error) {        
            responseHelper.returnError(res, 201, result.error.details)
        }
        var data = await bookService.create(req.body);        
        responseHelper.returnSuccess(res, 201, data);
    }

    async detail(req, res, next){     
        console.log(req.params.id)   ;
        if(req.params.id!=null)
        {
            var data = await bookService.findOne(req.params.id);            
        }
        responseHelper.returnSuccess(res, 201, data);
    };

    async update(req, res, next){
        var data;
        if(req.params.id!=null)
        {
            const result = Joi.validate(req.body, bookForm)            
            if(result.error) {        
                responseHelper.returnError(res, 201, result.error.details)
            }
            // console.log(req.params.id)                        
            data = await bookService.update(req.params.id, req.body);
        }
        
        responseHelper.returnSuccess(res, 201, data);
    };
    
    async delete(req, res, next){
        var data;
        if(req.params.id!=null)
        {
            const result = Joi.validate(req.body, bookForm)            
            if(result.error) {        
                responseHelper.returnError(res, 201, result.error.details)
            }
            // console.log(req.params.id)                        
            var data = await bookService.delete(req.params.id, req.body);
        }
        responseHelper.returnSuccess(res, 201, null);
    };
};

module.exports = BookController;




// exports.index = (req, res) => {
//     responseHelper.returnJson(req, res, 200, 200, 'NOT IMPLEMENTED', null);
// };

// exports.create = (req, res) =>{    
//     const result = Joi.validate(req.body, bookForm);    
//     if(result.error) {        
//         responseHelper.returnJson(res, 200, {
//             code:201,
//             message:'validation error',
//             details:result.error.details
//         });
//     }
//     responseHelper.returnJson(res, 200);

// };

// // Display detail page for a specific book.
// exports.detail = function(req, res) {
//     console.log(req.body.id);
//     console.log("detail")

//     responseHelper.returnJson(res, 400);
// };

// exports.update = function(req, res) {
//     const result = Joi.validate(req.body, bookForm);    
//     if(result.error) {        
//         responseHelper.returnJson(res, 200, {
//             code:201,
//             message:'validation error',
//             details:result.error.details
//         });
//     }

//     responseHelper.returnJson(res, 200);
// };

// exports.delete = function(req, res) {
//     responseHelper.returnJson(res, 200);
// };
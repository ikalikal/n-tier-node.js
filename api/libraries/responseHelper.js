var http = require('http');
exports.returnJson = (res, httpcode, meta = null, data = null) => {
    res.status(httpcode);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({
        'meta':meta || {code:200, message:'success'},
        'data':data || null
    }));    
};
exports.returnSuccess = (res, httpcode=200, data)=>{
    res.status(httpcode);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({
        'meta':{code:201, message:'success'},
        'data':data || null
    })); 
};
exports.returnError = (res, httpcode=201, data)=>{
    res.status(httpcode);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({
        'meta':{code:400, message:'failed'},
        'data':data || null
    }));  
};


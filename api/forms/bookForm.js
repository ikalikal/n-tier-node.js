const Joi = require('joi');

module.exports = Joi.object().keys({    
    title: [Joi.string().min(10).required()]
});


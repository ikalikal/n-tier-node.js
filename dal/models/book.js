'use strict';
module.exports = (Sequelize, DataTypes) => {
  const Book = Sequelize.define('books', {    
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    title: DataTypes.STRING,    
    is_deleted: DataTypes.INTEGER,    
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,    
  }, {
    updatedAt: 'updated_at',
    createdAt: 'created_at'
  });
  return Book;
};
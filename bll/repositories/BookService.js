"use strict";
const BookViewModel = require('../viewmodel/BookViewModel');
const bookModel = require('../../dal/models/book');
const db = require('../../dal/app.js');

class BookService{
    constructor(){

    }
    async getList(){                  
        // var data=[];
        // var raw = await db.books.findAll();
        // raw.forEach(element => {
        //     data.push(new BookViewModel.BookViewModel(element));
        // });       
        // return data;        
        var data=[];
        await db.books.findAll({
            where:{
                is_deleted:0
            }
        })
        .then(raw =>{
            raw.forEach(element => {
                data.push(new BookViewModel.BookViewModel(element));
            });       
        })
        .catch(()=>{
            return;
        })
        return data;
    }

    async create(rawData){    
        var data;          
        await db.books.create({  
            title:rawData.title
        }).then(raw => {                
            data = new BookViewModel.BookViewModel(raw);
        });

        return data;
    }

    async findOneRaw(id)
    {
        var data;
        await db.books.findOne({
            where:{
                id:id,
                is_deleted:0
            }
        })
        .then((raw)=>{
            if(raw!=null)
            {
                data = raw;
            }
        });
        return data;
    }

    async findOne(id)
    {
        var data;
        await this.findOneRaw(id)
        .then((raw)=>{
            if(raw!=null)
            {
                data = new BookViewModel.BookViewModel(raw);
            }
        });
        return data;
    }

    async update(id, rawData){
        var data;
        console.log(rawData);
        await this.findOneRaw(id)
        .then((raw)=>{
            raw.update({
                title:rawData.title,
                updated_at: new Date()                
            }) 
            console.log("updated");
            return raw;                   
        })
        .then((raw)=>{            
            console.log("converted");
            console.log(raw);
            data = new BookViewModel.BookViewModel(raw); 
        })
        .catch((error)=>{
            console.log(error);
        });        
        return data;

    }

    async delete(id)
    {
        var data;        
        await this.findOneRaw(id)
        .then((raw)=>{
            raw.update({
                is_deleted:1,
                updated_at: new Date()                
            })                                       
        })        
        .catch((error)=>{
            console.log(error);
        });        
        return;
    }
}

module.exports.BookService = BookService;